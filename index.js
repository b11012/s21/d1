// alert("hi");
// Array
/*
    - Arrays are used to store multiple related values in a single variable
    - They are declared using square brackets ([]) also known as "Array Literals"
    - Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
    - Arrays also provide access to a number of functions/methods that help in achieving this
    - A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
    - Majority of methods are used to manipulate information stored within the same object
    - Arrays are also objects which is another data type
    - The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
 */

// Array Traversal

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// Put it in an array
/*
	Syntax: let/ const arrayName = [elementA, elementB, ...]
*/

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

// Common examples of an array
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Alienware"];

console.log(grades);
console.log(computerBrands);

// Alternative ways of writing an array

let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake react'
];
console.log(myTasks);

// Create an array with values from variables

let city1 = "Tokyo";
let city2 = "Jakarta";
let city3 = "Manila";

let cities = [city1, city2, city3];
console.log(cities);

// .length property
//The .length property allows us to get and set the total number of items in an array.

console.log(myTasks.length);
// result: 4

console.log(cities.length);
// result: 3

//length property can also be used with strings. Some array methods and properties can also be used with strings.
let fullName = "Lisa Manoban";
console.log(fullName.length);
// result: 12 including the white space


//length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array.
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);
// result: removed bake react

// another example using decrementation
cities.length--;
console.log(cities);
// result: removed Manila

// we can't do the same with strings
fullName.length = fullName.length-1;
console.log(fullName.length)
// result: still 12

fullName.length--;
console.log(fullName.length);
// result: still 12

//If you can shorten the array by setting the length property, you can also lengthen it by adding a number into the length property. Since we lengthen the array forcibly, there will be another item in the array, however, it will be empty or undefined. Like adding another seat but having no one to sit on it.
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);
// result: empty / undefined

// Reading from Arrays
/*
    - Accessing array elements is one of the more common tasks that we do with an array
    - This can be done through the use of array indexes
    - Each element in an array is associated with it's own index/number
    - In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
    - The reason an array starts with 0 is due to how the language is designed
	Syntax:
		arrayName[index];
*/
// index starts with 0
console.log(grades[0]);
console.log(computerBrands[3]);

console.log(grades[20]);
// result: undefined

let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
// result: Shaq
console.log(lakersLegends[3]);
// result: Magic

// assigning items in another variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);
// result: LeBron

// reassigning

console.log("Array before reassignment: ");
console.log(lakersLegends);
console.log("Array after reassignment: ")
lakersLegends[3] = "Gasol";
console.log(lakersLegends);

// accessing the last element of an array
//Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element.
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
console.log(bullsLegends.length);
// number of elements = 5
	// this starts counting at 1

// total number of index = 4
	// this starts at 0

let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex])
// result: Kukoc

//You can also add it directly:
console.log(bullsLegends[bullsLegends.length-1]);

// Adding items into the array
let newArr = [];
console.log(newArr[0]);
// result: undefined

//newArr[0] is undefined because we haven't yet defined the item/data for that index, we can update the index with a new value:
newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[2] = "Zack Fair";
console.log(newArr);

//You can also add items at the end of the array. Instead of adding it in the front to avoid the risk of replacing the first items in the array:
newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);
//newArr[3] = "Barrett"

newArr[newArr.length-1] = "Aerith Gainsborough"

// reassigning elements in array using const
const family = ["Edward", "Alphonse", "dog"]
console.log("Current Family: " + family)

family[2] = "Nina";
console.log("New Family: " + family)

// Looping over an Array
//You can use a for loop to iterate over all items in an array.
//Set the counter as the index and set a condition that as long as the current index iterated is less than the length of the array, we will run the condition. It is set this way because the index of an array starts at 0.
for(let index = 0; index < newArr.length; index++){

	//You can use the loop counter as index to be able to show each array items in a console log.
	console.log(newArr[index]);
};


//Given an array of numbers, you can also show if the following items in the array are divisible by 5 or not. You can mix in an if-else statement in the loop:

let numArr = [5, 12, 30, 46, 60];

for(let index = 0; index < numArr.length; index++){

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	} else {
		console.log(numArr[index] + " is not divisible by 5")
	};

};

// Multidimensional Array
/*
    - Multidimensional arrays are useful for storing complex data structures
    - A practical application of this is to help visualize/create real world objects
    - Though useful in a number of cases, creating complex array structures is not always recommended.
*/

let chessBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]
];

console.log(chessBoard);
console.log("Pawn moves to: " + chessBoard[1][5])
